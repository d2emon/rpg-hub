import themes, { Theme } from './themes';

export interface ProjectConfig {
  api: {
    baseURL: string;
    timeout?: number;
  };
  iconfont?: 'mdi' | 'mdiSvg' | 'md' | 'fa' | 'fa4' | 'faSvg';
  lang?: string;
  theme?: {
    dark?: boolean;
    themes: { [k: string]: Theme };
  };
}

const config: ProjectConfig = {
  api: {
    baseURL: 'http://127.0.0.1:4000/api/v1.0/',
    timeout: 1000,
  },
  iconfont: 'mdi',
  lang: 'ru',
  theme: {
    dark: false,
    themes,
  },
};

export default config;
