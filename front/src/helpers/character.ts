export interface Character {
  id?: string;
  name?: string;
  description?: string;
  aspects?: string[];
  additional?: string;
  stress?: string;
  updates?: number;
  skills?: string[];
  tricks?: string;
  consequences?: string;
}

export interface CharacterId {
  partyId: string;
  characterId: string;
}
