import { Character } from '@/helpers/character';

export interface Party {
  id: string;
  characters: Character[];
  name: string;
}

const parties: Party[] = [
  {
    id: 'fate',
    characters: [
      {
        id: 'character-1',
        name: 'Имя',
      },
      {
        id: 'sainer',
        name: 'Сайнер',
        aspects: [
          'Обожает побрякушки',
        ],
      },
      {
        id: 'zird',
        name: 'Зирд',
        skills: [
          'Знания (+4)',
        ],
      },
      {
        id: 'landon',
        name: 'Лэндон',
        tricks: 'Еще по одной',
      },
    ],
    name: 'Fate 1',
  },
  {
    id: 'party-2',
    characters: [],
    name: 'Crew 2',
  },
];

export default parties;
