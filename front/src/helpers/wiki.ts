export interface WikiPage {
  id?: string;
  author?: string;
  slug?: string;
  subtitle?: string;
  title?: string;
  translatedBy?: string;
  url?: string;
}
