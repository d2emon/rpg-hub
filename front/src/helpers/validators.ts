export const required = (
  message = 'Поле обязательно для заполнения',
) => (v: string) => (!!v || message);

export const maxLength = (
  count = 10,
  message = 'Поле слишком длинное',
) => (v: string) => ((v && v.length <= count) || message);
