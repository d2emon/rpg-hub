import MarkdownIt from 'markdown-it';

const markdown = MarkdownIt({
  html: true,
}).enable(['table']);

export default markdown;
