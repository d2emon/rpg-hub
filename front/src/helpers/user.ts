import { Notification } from '@/helpers/notification';

export interface User {
  avatar?: string;
  name: string;
  notifications?: Notification[];
  profileLink?: string;
  rank?: string;
  username: string;
}
