export interface Notification {
  id: string;
  message: string;
  link?: string;
}
