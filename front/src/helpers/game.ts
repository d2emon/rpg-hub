export interface Threats {
  actual: string[];
  emerging: string[];
}

export interface Person {
  name: string;
  aspect: string;
}

export interface Game {
  name: string;
  setting: string;
  scale: string;
  threats: Threats;
  persons: Person[];
}
