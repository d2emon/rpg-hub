import axios from 'axios';
import config from '@/config';
import { User } from '@/helpers/user';
import parties, { Party } from '@/helpers/party';
import { WikiPage } from '@/helpers/wiki';

const api = axios.create(config.api);

export default {
  getUser: (userId: string): Promise<User | null> => api.get(`user/${userId}`)
    .then((response) => response.data)
    .then((data) => data.result)
    .then((user: User | null) => (user && {
      ...user,
      avatar: user.avatar || 'https://randomuser.me/api/portraits/men/81.jpg',
      notifications: user.notifications && user.notifications.map((notification) => ({
        ...notification,
        link: `/user/${user.username}/notification/${notification.id}`,
      })),
      profileLink: `/user/${user.username}`,
      rank: 'GM',
    })),

  getWikiPages: (): Promise<WikiPage[] | null> => api.get('wiki')
    .then((response) => response.data)
    .then((data) => data.result)
    .then((pages: WikiPage[] | null) => (pages && pages.map((page) => ({
      ...page,
      url: `/wiki/${page.slug}`,
    })))),
  addWikiPage: (page: WikiPage): Promise<any> => api.post('wiki', page)
    .then((response) => response.data)
    .then((data) => data.result),
  getWikiPage: (pageId: string): Promise<string | null> => api.get(`wiki/${pageId}`)
    .then((response) => response.data),
  setWikiPage: (pageId: string, page: WikiPage): Promise<any> => api.put(`wiki/${pageId}`, page)
    .then((response) => response.data)
    .then((data) => data.result),
  delWikiPage: (pageId: string): Promise<any> => api.delete(`wiki/${pageId}`)
    .then((response) => response.data)
    .then((data) => data.result),

  getParties: (): Promise<Party[] | null> => Promise.resolve(parties),
  getParty: (partyId: string): Promise<Party | null> => Promise
    .resolve(parties.find((p) => (p.id === partyId)) || null),
};
