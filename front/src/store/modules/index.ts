import { ModuleTree } from 'vuex';
import { RootState } from '@/store/state';
import party from './party';

const modules: ModuleTree<RootState> = {
  party,
};

export default modules;
