import Vue from 'vue';
import { MutationTree } from 'vuex';
import { Party } from '@/helpers/party';
import { Character } from '@/helpers/character';
import { PartyState } from './state';

const mutations: MutationTree<PartyState> = {
  setParties: (state: PartyState, value?: Party[]) => Vue.set(state, 'parties', value),
  setParty: (state: PartyState, value?: Party) => Vue.set(state, 'party', value),
  setCharacter: (state: PartyState, value?: Character) => Vue.set(state, 'character', value),
};

export default mutations;
