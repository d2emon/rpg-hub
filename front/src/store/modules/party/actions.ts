import { ActionTree } from 'vuex';
import api from '@/helpers/api';
import {
  Character,
  CharacterId,
} from '@/helpers/character';
import { Party } from '@/helpers/party';
import { RootState } from '@/store/state';
import { PartyState } from './state';

const actions: ActionTree<PartyState, RootState> = {
  fetchParties: async ({ commit }): Promise<Party[] | null> => api.getParties()
    .then((value: Party[] | null) => {
      commit('setParties', value);
      return value;
    }),
  fetchParty: async ({ commit }, id: string): Promise<Party | null> => api.getParty(id)
    .then((value: Party | null) => {
      commit('setParty', value);
      return value;
    }),
  fetchCharacter: async ({ commit }, payload: CharacterId): Promise<Character | null> => api
    .getParty(payload.partyId)
    .then((value: Party | null) => {
      commit('setParty', value);
      return value;
    })
    .then((party: Party | null) => {
      const character: Character | null = (party && party.characters
        .find((c) => (c.id === payload.characterId))) || null;
      commit('setCharacter', character);
      return character;
    }),
};

export default actions;
