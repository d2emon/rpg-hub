import { Party } from '@/helpers/party';
import { Character } from '@/helpers/character';

export interface PartyState {
  parties?: Party[];
  party?: Party;
  character?: Character;
}

const state: PartyState = {
  parties: [],
};

export default state;
