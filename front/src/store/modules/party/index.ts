import { Module } from 'vuex';
import { RootState } from '@/store/state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import state, { PartyState } from './state';

const store: Module<PartyState, RootState> = {
  actions,
  getters,
  mutations,
  namespaced: true,
  state,
};

export default store;
