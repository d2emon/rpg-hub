import { ActionTree } from 'vuex';
import { RootState } from '@/store/state';
import api from '@/helpers/api';
import { User } from '@/helpers/user';
import { WikiPage } from '@/helpers/wiki';

const actions: ActionTree<RootState, RootState> = {
  fetchUser: async ({ commit }, userId: string): Promise<User | null> => api
    .getUser(userId)
    .then((user: User | null) => {
      commit('setUser', user);
      return user;
    }),

  fetchWikiPages: async ({ commit }): Promise<WikiPage[] | null> => api
    .getWikiPages()
    .then((text: WikiPage[] | null) => {
      commit('setWikiPages', text);
      return text;
    }),
  addWikiPage: async ({ commit }, page: WikiPage): Promise<any> => api
    .addWikiPage(page),
  fetchWikiPage: async ({ commit }, pageId: string): Promise<string | null> => api
    .getWikiPage(pageId)
    .then((text: string | null) => {
      commit('setWikiPage', text);
      return text;
    }),
  saveWikiPage: async ({ commit }, page: WikiPage): Promise<any> => (page.id
    ? api.setWikiPage(page.id, page)
    : api.addWikiPage(page)),
};

export default actions;
