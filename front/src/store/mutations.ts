import Vue from 'vue';
import { MutationTree } from 'vuex';
import { RootState } from '@/store/state';
import { User } from '@/helpers/user';
import { WikiPage } from '@/helpers/wiki';

const mutations: MutationTree<RootState> = {
  setUser: (state: RootState, value?: User) => Vue.set(state, 'user', value),
  setWikiPage: (state: RootState, value?: string) => Vue.set(state, 'wikiPage', value),
  setWikiPages: (state: RootState, value?: WikiPage[]) => Vue.set(state, 'wikiPages', value),
};

export default mutations;
