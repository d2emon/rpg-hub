import { User } from '@/helpers/user';
import { WikiPage } from '@/helpers/wiki';

interface Threats {
  actual: string;
  emerging: string;
}

export interface RootState {
  ladder: { [key: number]: string };
  threats: Threats;
  title: string;
  user?: User;
  wikiPage?: string;
  wikiPages?: WikiPage[];
}

const state: RootState = {
  ladder: {
    8: 'Легендарный',
    7: 'Эпический',
    6: 'Фантастический',
    5: 'Великолепный',
    4: 'Отличный',
    3: 'Хороший',
    2: 'Неплохой',
    1: 'Средний',
    0: 'Посредственный',
    '-1': 'Плохой',
    '-2': 'Ужасный',
  },
  threats: {
    actual: 'Насущные угрозы',
    emerging: 'Назревающие угрозы',
  },
  title: 'RPG Hub',
};

export default state;
