import express from 'express';
import handlers from '../handlers/wiki';

const router = express.Router();

router.get('/', handlers.getItems);
router.post('/', handlers.addItem);
router.get('/:filename', handlers.getItem);
router.put('/:id', handlers.updateItem);
router.delete('/:id', handlers.removeItem);

export default router;
