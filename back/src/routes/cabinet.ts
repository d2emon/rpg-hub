import express from 'express';
import handlers from '../handlers/user';

const router = express.Router();

router.get('/', handlers.getItems);
router.post('/', handlers.addItem);
router.get('/id/:id', handlers.getItem);
router.get('/:username', handlers.getItemByUsername);
router.put('/:id', handlers.updateItem);
router.delete('/:id', handlers.removeItem);

export default router;
