import {
    Request,
    Response,
} from 'express';

export type HandleCallback = (req: Request, res: Response) => any;

const handler = (callback: HandleCallback) => async (req: Request, res: Response) => {
    try {
        const result = await callback(req, res);
        return res.json({ result });
    } catch (e) {
        return res.status(500).json({ error: e.message });
    }
};

export default handler;
