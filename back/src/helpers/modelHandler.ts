import {
    Request,
    Response,
} from 'express';
import {
    Model,
} from 'mongoose';

export interface CRUDHandler {
    getItems: (req: Request, res: Response) => Promise<Response>;
    addItem: (req: Request, res: Response) => Promise<Response>;
    getItem: (req: Request, res: Response) => Promise<Response>;
    getItemBySlug: (req: Request, res: Response) => Promise<Response>;
    updateItem: (req: Request, res: Response) => Promise<Response>;
    removeItem: (req: Request, res: Response) => Promise<Response>;
}

/**
 * Обработчики запросов на создание, просмотр, редактирование и удаление записей в БД
 * @param model - модель
 * @returns CRUDHandler
 */
export default (model: Model<any>): CRUDHandler => ({
    /**
     * Обработчик запроса на получение списка записей
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    getItems: async (req: Request, res: Response) => {
        try {
            const query = {};
            const result = await model.find(query);
            return res.json({ result });
        } catch (e) {
            return res.status(500).json({ error: e.message });
        }
    },

    /**
     * Обработчик запроса на добавление записи
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    addItem: async (req: Request, res: Response) => {
        try {
            const record = new model(req.body);
            const result = await record.save();
            return res.json({ result });
        } catch (e) {
            return res.status(500).json({ error: e.message });
        }
    },

    /**
     * Обработчик запроса на получение одной записи по идентификатору
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    getItem: async (req: Request, res: Response) => {
        try {
            const result = await model.findById(req.params.id);
            return res.json({ result });
        } catch (e) {
            return res.status(500).json({ error: e.message });
        }
    },

    /**
     * Обработчик запроса на получение записи по человекопонятной ссылке
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    getItemBySlug: async (req: Request, res: Response) => {
        try {
            const result = await model.findOne({ slug: req.params.slug });
            return res.json({ result });
        } catch (e) {
            return res.status(500).json({ error: e.message });
        }
    },

    /**
     * Обработчик запроса на обновление записи
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    updateItem: async (req: Request, res: Response) => {
        try {
            const result = await model.findByIdAndUpdate(req.params.id, req.body);
            return res.json({ result });
        } catch (e) {
            return res.status(500).json({ error: e.message });
        }
    },

    /**
     * Обработчик запроса на удаление записи
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    removeItem: async (req: Request, res: Response) => {
        try {
            const result = await model.findByIdAndDelete(req.params.id);
            return res.json({ result });
        } catch (e) {
            return res.status(500).json({ error: e.message });
        }
    }
});
