/**
 * Настройки для серверной части
 */
import path from 'path';

export interface Config {
    API_PATH: string;
    APP_NAME: string;
    // CLIENT_ID: string;
    // CLIENT_SECRET: string;
    LOG_FILENAME?: string;
    LOG_LEVEL: string;
    MONGO_URI: string;
    PORT: string | number;
    PUBLIC_PATH: string;
    // TOKEN_LIFETIME: string | number;
    WIKI_IMAGES: string;
    WIKI_PATH: string;
}

const basePath = path.join(__dirname, '..', '..');

const config: Config = {
    API_PATH: process.env.API_PATH || '/api/v1.0',
    APP_NAME: process.env.APP_NAME || 'rpg-hub',
    // CLIENT_ID: process.env.CLIENT_ID || 'augustClient',
    // CLIENT_SECRET: process.env.CLIENT_SECRET || 'thereisnospoon',
    LOG_FILENAME: process.env.LOG_FILENAME,
    LOG_LEVEL: process.env.LOG_LEVEL || 'info',
    MONGO_URI: process.env.MONGO_URI || 'mongodb://127.0.0.1:27017/august',
    PORT: process.env.PORT || 4000,
    PUBLIC_PATH: process.env.PUBLIC_PATH || path.join(basePath, 'public'),
    // TOKEN_LIFETIME: process.env.TOKEN_LIFETIME || (30 * 60),
    WIKI_IMAGES: process.env.WIKI_IMAGES || '/wiki/images',
    WIKI_PATH: process.env.WIKI_PATH || path.join(basePath, 'pages'),
};

export default config;
