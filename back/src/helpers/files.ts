import fs from 'fs';

export const readDir = (path: string) => new Promise<string[]>((resolve, reject) => {
    fs.readdir(path, (err, files) => {
        if (err) {
            return reject(err);
        }
        return resolve(files);
    });
});

export const readFile = (path: string) => new Promise<string>((resolve, reject) => {
    try {
        if (!fs.existsSync(path)) {
            return resolve('');
        }
        fs.readFile(path, 'utf8', (err, text) => {
            if (err) {
                return reject(err);
            }
            return resolve(text);
        });
    } catch (e) {
        return reject(e)
    }
});
