import winston from 'winston'
import config from './config';

const transports: any[] = [
    new winston.transports.Console({
        format: winston.format.simple(),
        level: config.LOG_LEVEL,
    }),
];

if (config.LOG_FILENAME) {
    transports.push(new winston.transports.File({
        filename: config.LOG_FILENAME,
        format: winston.format.json(),
        level: config.LOG_LEVEL,
    }));
}

const logger = winston.createLogger({
    transports,
});

export default logger;
