import mongoose, {
    Document,
    Model,
    Schema,
} from 'mongoose';

export interface IWikiPageData {
    id?: any;
    author?: string;
    slug: string;
    subtitle?: string;
    title: string;
    translatedBy?: string;
}

export interface IWikiPageDocument extends Document, IWikiPageData {}

export interface IWikiPageModel extends Model<IWikiPageDocument> {}

const WikiPageSchema = new Schema({
    author: {
        type: String,
    },
    subtitle: {
        type: String,
    },
    title: {
        type: String,
        required: true,
    },
    translatedBy: {
        type: String,
    },
});

WikiPageSchema.virtual('slug').get(function (): string {
    // return encodeURI(`${this.title}.md`);
    return `${this.title}.md`;
})

WikiPageSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: (doc, ret) => {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
    },
});

const WikiPage: IWikiPageModel = mongoose.model<IWikiPageDocument, IWikiPageModel>('WikiPage', WikiPageSchema);

export default WikiPage;
