import mongoose, {
    Document,
    Model,
    Schema,
} from 'mongoose';

export interface INotificationDocument extends Document {
    message: string;
}

export interface INotificationModel extends Model<INotificationDocument> {}

export const NotificationSchema = new Schema({
    message: {
        type: String,
        required: true,
    },
});

NotificationSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: (doc, ret) => {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
    },
});

const Notification: INotificationModel = mongoose.model<INotificationDocument, INotificationModel>('Notification', NotificationSchema);

export default Notification;
