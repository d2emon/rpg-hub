import mongoose, {
    Document,
    Model,
    Schema,
} from 'mongoose';
import Notification, {
    INotificationDocument,
    NotificationSchema,
} from './notification';

export interface IUserDocument extends Document {
    username: string;
    name: string;
    avatar: string;
    notifications: INotificationDocument[];
    notify: (message: string) => null;
}

export interface IUserModel extends Model<IUserDocument> {}

const UserSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique: true,
    },
    name: {
        type: String,
        required: true,
    },
    avatar: {
        type: String,
    },
    notifications: {
        type: [NotificationSchema],
    }
});

UserSchema.methods.notify = function (this: IUserDocument, message: string) {
    this.notifications.push(new Notification({ message }));
};

UserSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: (doc, ret) => {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
    },
});

const User: IUserModel = mongoose.model<IUserDocument, IUserModel>('User', UserSchema);

export default User;
