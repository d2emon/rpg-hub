import mongoose, {
    Document,
    Model,
    Schema,
} from 'mongoose';

export interface IGameDocument extends Document {
    name: string;
    slug: string;
}

export interface IGameModel extends Model<IGameDocument> {}

const GameSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    slug: {
        type: String,
        required: true,
    },
});

GameSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: (doc, ret) => {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
    },
});

const Game: IGameModel = mongoose.model<IGameDocument, IGameModel>('Game', GameSchema);

export default Game;
