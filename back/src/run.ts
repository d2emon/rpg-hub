import app from './app';
import config from './helpers/config';
import debug from './helpers/debug';
import normalizePort from './helpers/normalizePort';

app.set('port', normalizePort(config.PORT));

app.listen(
    app.get('port'),
    () => debug('run')(`Express server listening on port ${config.PORT}`),
)
