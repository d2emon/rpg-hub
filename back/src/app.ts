import cors from 'cors';
import express from 'express';
import morgan from 'morgan';

import {
    error404,
    errorHandler,
} from './handlers/error';
import config from './helpers/config';
import debug from './helpers/debug';
import mongoDb, { connect } from './helpers/mongo';

import gameRoutes from './routes/game';
import pageRoutes from './routes/page';
import userRoutes from './routes/user';
import wikiRoutes from './routes/wiki';

const app = express();

debug('config')(JSON.stringify(config));

app.use(morgan('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(config.PUBLIC_PATH));

app.set('dbConnection', connect(config.MONGO_URI))

mongoDb.on('error', error => debug('db')(error || ''));
mongoDb.once('open', () => debug('db')('MongoDB connected'));

app.use(`${config.API_PATH}/game`, gameRoutes);
app.use(`${config.API_PATH}/page`, pageRoutes);
app.use(`${config.API_PATH}/user`, userRoutes);
app.use(`${config.API_PATH}/wiki`, wikiRoutes);

app.use(error404);
app.use(errorHandler(app.get('env')));

export default app;
