import {
    Request,
    Response,
} from 'express';
import config from '../helpers/config';
import {
    readDir,
    readFile,
} from '../helpers/files';
import handler from '../helpers/handler';
import WikiPage, { IWikiPageData } from '../models/wikiPage';

export default {
    getItems: handler(async () => {
        const query = {};
        const data = await WikiPage.find(query).sort('title');
        const result: IWikiPageData[] = [ ...data ];

        const fileNames = await readDir(config.WIKI_PATH)
            .then((files) => files.filter((file) => /^(.*)\.md$/.test(file)))
            .then((files) => files.map((file) => file.replace(/^(.*)\.md$/, '$1')));
        fileNames.forEach((title) => {
            const found = result.find((page) => (page.title === title));
            if (!found) {
                result.push({
                    // slug: encodeURI(`${title}.md`),
                    slug: `${title}.md`,
                    title,
                });
            }
        });

        return result;
    }),
    addItem: handler(async (req: Request) => {
        const record = new WikiPage(req.body);
        return await record.save();
    }),
    getItem: async (req: Request, res: Response) => {
        try {
            const filename = `${config.WIKI_PATH}/${req.params.filename}`
            let text = await readFile(filename) || '';
            text = text.replace(/!\[(.*?)]\(\.\/images\/(.*?)\)/g, `![$1](${config.WIKI_IMAGES}/$2)`);
            return res.send(text);
        } catch (e) {
            return res.status(500).send(e.message);
        }
    },
    updateItem: handler((req: Request) => WikiPage.findByIdAndUpdate(req.params.id, req.body)),
    removeItem: handler((req: Request) => WikiPage.findByIdAndDelete(req.params.id, req.body)),
};
