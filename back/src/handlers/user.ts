import {
    Request,
    Response,
} from 'express';
import User from '../models/user';
import modelHandler from '../helpers/modelHandler';

const handler = modelHandler(User);

export default {
    getItems: handler.getItems,
    addItem: handler.addItem,
    getItem: handler.getItem,
    updateItem: handler.updateItem,
    removeItem: handler.removeItem,
    getItemByUsername: async (req: Request, res: Response) => {
        try {
            const result = await User.findOne({ username: req.params.username });
            return res.json({ result });
        } catch (e) {
            return res.status(500).json({ error: e.message });
        }
    },
    notifyUser: async (req: Request, res: Response) => {
        try {
            const result = await User.findOne({ username: req.params.username });
            result.notify('Test message');
            await result.save();
            return res.json({ result });
        } catch (e) {
            return res.status(500).json({ error: e.message });
        }
    },
};
