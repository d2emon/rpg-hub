import {
    Request,
} from 'express';
import handler from '../helpers/handler';
import Game from '../models/game';

const games = [
    /*
* [Пиратка](./Пиратка)
* 13th Age
* 1PG
* 7th Sea
* [Alternity](./Alternity)
* Amazing Engine
* Amber
* Apocalypse Prevention, Inc.
* Apocalypse World
* Arcanum
* [**Basic Role-Playing**](./Basic%20Role-Playing)
* Beast Hunters
* Big Eyes, Small Mouth
* Black Crusade
* Blackbird Pie
* Bones
* Boot Hill
* Bulldogs!
*   [Bunnies & Burrows](./Bunnies%20&%20Burrows)
*   [**Call of Cthulhu**](./Call%20of%20Cthulhu)
* Chill
* Cold City
* Conan Role-Playing Game
*   [Council of Wyrms](./Council%20of%20Wyrms)
*   [Creature Crucible](./Creature%20Crucible)
*   [**Cyberpunk 2020**](./Cyberpunk%202020)
* Cyborg Commando
* **D20 Modern**
* Dangerous Journeys
* Dark Dungeons
* Dark Heresy
* Das Schwarze Auge
* Deadlands
* Deathwatch
* Dogs in the Vineyard
* Don't Rest Your Head
* Dragon Age
*   [Dragon Fist](./Dragon%20Fist)
* Dresden Files
* Dungeon World
*   [Dungeons & Dragons](./Dungeons%20&%20Dragons)
* Edge of the Empire
* Eldritch Role-Playing
* Fallout
* Familiars
* Fantasy Craft
* FATAL
*   [**FATE**](./FATE)
    *   Fate Accelerated Edition
* Fireball
* FUBAR
*   [**FUDGE**](./FUDGE)
*   [**Fuzion**](./Fuzion)
*   [**Gamma World**](./Gamma%20World)
* Gangbusters
* Ghostbusters
* GNS
* Grey Ranks
* Grimm
* GUMSHOE
*   [**GURPS**](./GURPS)
* HeroQuest
* HoL
* Houses of the Blooded
* ICON
* Icons
* In a Wicked Age
* Justifiers RPG
* Kill puppies for satan
* Kobolds Ate My Baby!
* Labyrinth Lord
* Lady Blackbird
* Lejendary Adventure
* Lords of Creation
* Megaversal System
* Mekton
* Metamorphosis Alpha
* Microcosm
* Middle-earth Role Playing
* Monster of the Week
*   [Munchkin](./Munchkin)
* My Life With Master
* Mythender
* Mythic Role Playing
* Nicotine Girls
* No dice engine
* Numenera
* OSRIC
* One-Roll Engine
* Original Dungeons & Dragons
* Original Edition Delta
* Over the Edge
* Panty Explosion Perfect
*   [**Paranoia**](./Paranoia)
*   [**Pathfinder**](./Pathfinder)
* PDQ
*   [**Pendragon**](./Pendragon)
* Pirates and Plunder
* Planets of Peril
* Polyhedral Dungeon
* Primetime Adventures
* Prince Valiant
* Racial Holy War
* Ribbon Drive
* Rogue Trader
* Rolemaster
* RuneQuest
* S.P.E.C.I.A.L.
* S/Lay w/Me
* SAGA System
*   [**Savage Worlds**](./Savage%20Worlds/index.md)
* Sellsword
*   [**Shadowrun**](./Shadowrun)
* Sherpa
* Shonen Final Burst
* SLURPS
* Sorcerer
* Spellbound Kingdoms
* Sphinx (игра)
* Star Frontiers
* Star Trek: The Original Series RPG
* Star Trek RPG
* Star Wars Roleplaying Game (Wizards of the Coast)
* StickGuy
* Stormbringer
* Storytelling
* Swordbearer
* The Black Hack
* The Burning Wheel
* The Challenges Game System
* The Fantasy Trip
* The Riddle of Steel
* The Shadow of Yesterday
* The World, the Flesh, and the Devil
* Torchbearer
* Traveller
* Tri-Stat dX
* Trollbabe
* True20
* Twisted Terra
* Unhallowed Metropolis
* Violence
* Warcraft RPG
* Warhammer
* Warhammer Fantasy Roleplay
* Weird Fantasy Role-Playing
* Winnie-the-Owl-Pooh
* Woof Meow
*   [World of Darkness](./World%20of%20Darkness)
* World of Warcraft RPG
* Worlds of Wonder
*   [**АРРРГХЪ!**](./АРРРГХЪ!)
* Альшард
* Арианрод
* Ведьмак: Игра воображения
* Воспоминания о будущем
* Горная ведьма
* Дзайбацу
*   [**Заколдованная страна**](./Заколдованная%20страна)
* Замок Фалькенштайн
* Иномирье
*   [**Искусство волшебства**](./Искусство%20волшебства)
* Колония
* Лавикандия
*   [Лексикон](./Лексикон)
* Лорды
* Марс: Новый воздух
* Мир Великого Дракона
* Монстробой
* Муршамбала
*   [**Неизвестные армии**](./Неизвестные%20армии)
* Паладин (игра)
* Перекрёсток миров
* Ризус
* Система D6
* **Система d20**
* ТриМ
* Триплет
* Универсалис
*   [**Уникум**](./Уникум)
* Уроборос
* Фен Шуй
* Фиаско
* Фронт
*   [Хентаи](./Хентаи)
* Хиккори Диккори Док!
* Чики-чики-ща
* Штамм 33
*   [**Эра Водолея**](./Эра%20Водолея)
     */

    /*
1.  [Dungeons & Dragons](./Dungeons%20&%20Dragons)
1.  [Shadowrun](./Shadowrun)
1.  [Call of Cthulhu](./Call%20of%20Cthulhu)
1.  Vampire: The Masquerade
1.  Star Wars The Roleplaying Game
1.  [Pathfinder](./Pathfinder)
1.  [GURPS](./GURPS)
1.  Advanced Dungeons & Dragons First Edition
1.  Advanced Dungeons & Dragons Second Edition
1.  Werewolf: The Apocalypse
1.  Mage: The Ascension
1.  [Savage Worlds](./Savage%20Worlds/index.md)
1.  [Paranoia](./Paranoia)
1.  Dungeons & Dragons (3rd Edition)
1.  Dungeons & Dragons 3.5
1.  Deadlands
1.  [Cyberpunk 2020](./Cyberpunk%202020)
1.  [World of Darkness](./World%20of%20Darkness)
1.  Dungeons and Dragons (5th Edition)
1.  Traveller
1.  Legend of the Five Rings
1.  Warhammer RPG
1.  [Gamma World](./Gamma%20World)
1.  [FATE](./FATE)
1.  Rifts
1.  Ars Magica
1.  RuneQuest
1.  Champions
1.  Middle-earth Role Playing
1.  Numenera
1.  Mutants and Masterminds
1.  Hero System
1.  Firefly
1.  Seventh Sea
1.  Marvel Super-heroes
1.  MechWarrior
1.  Changeling: The Dreaming
1.  Star Wars: Edge of the Empire
1.  Exalted
1.  Dark Heresy
1.  Vampire: the Dark Ages
1.  Robotech
1.  [Pendragon](./Pendragon)
1.  B/X Dungeons & Dragons
1.  Earthdawn
1.  Changeling: The Lost
1.  Classic BattleTech
1.  Mage: The Awakening
1.  Rolemaster
1.  Warhammer Fantasy Roleplay (1st Edition)
1.  Warhammer 40,000 Roleplay
1.  Call of Cthulhu (6th Edition)
1.  Fiasco
1.  Star Wars Roleplaying Game Saga Edition
1.  Toon the Cartoon Roleplaying Game
1.  Stormbringer
1.  Palladium Fantasy Role-Playing Game
1.  [Munchkin](./Munchkin)
1.  Top Secret
1.  GURPS Cyberpunk
1.  HeroQuest
1.  The Dresden Files Roleplaying Game
1.  Teenage Mutant Ninja Turtles & Other Strangeness
1.  Delta Green
1.  Kult
1.  Apocalypse World
1.  Vampire: The Requiem
1.  Wraith: the Oblivion
1.  Twilight 2000
1.  Boot Hill
1.  The One Ring
1.  Amber Diceless Roleplaying Game
1.  Dragon Age
1.  Trail of Cthulhu
1.  Trevas
1.  Toon
1.  Star Frontiers
1.  Villains & Vigilantes
1.  Battletech
1.  13th Age
1.  Dungeon World
1.  Castle Falkenstein
1.  Fading Suns
1.  Car Wars
1.  Arkanun
1.  Tagmar
1.  d20 Modern
1.  Scion
1.  Eclipse Phase
1.  Tunnels and Trolls
1.  Hunter: The Reckoning
1.  James Bond 007
1.  Iron Kingdoms
1.  Hackmaster 5e
1.  DC Heroes
1.  Doctor Who: Adventures in Time and Space
1.  Spelljammer, Ad&d 2nd Ed
1.  Aberrant
1.  Werewolf: The Forsaken
1.  All Flesh Must Be Eaten
     */

    /*
13th Age (Тринадцатый век)'
All Flesh Must Be Eaten (Вся плоть должна быть сожрана)'
Ars Magica (Искусство Волшебства)'
BattleTech
Best Left Buried (Лучше этого не знать)'
Blood star.pdf'
Call of Cthulhu (Зов Ктулху)'
Cyberpunk
Dark Souls'
Degenesis
Delta Green'
Don’t Rest Your Head.pdf'
Dragon Age (Век Дракона)'
DUNA (Дюна)'
Dungeon & Dragons (Подземелья и драконы)'
Equestrian Watch (Конверсия Deathwatch под My Little pony).pdf'
Fairyforest (Лес Фей)'
Fallout dS'
Fallout Pencil and Paper'
Fate
FRAGNAROK.pdf
FUDGE
Game of Thrones (Игры Престолов).doc'
GAMMASLAYERS.pdf
Godbound
GoldenSkyStories.pdf
Gumshoe (СЫЩИК)'
GURPS
HeXXen
Knave.pdf
Lamentations of the Flame Princess (Плач Огненной Принцессы)'
Legend of the Five Rings (Легенда Пяти Колец)'
MAFIA (Мафия).pdf'
Marvel Heroic Roleplaying (Марвел Героическая Игра)'
Mazes & Minotaurs (Лабиринты и минотавры)'
Monster_Slayers_v3.pdf
Mothership.pdf
Mouse Guard'
Numenera
One_day_in_Paris.pdf
Ouroboros
Pathfinder
Pdq#.pdf
Powered by the Apocalypse'
Rooms&Corridors (Комнаты и коридоры).pdf'
Shadowrun
Skyfarer
Starfinder
Star Trek (Звездный Путь)'
Star Wars (Звездные войны)'
Storytelling System (Мир Тьмы)'
The Dark Eye (Темное Око)'
UFO (Вторжение).pdf'
Unknown Armies (Неизвестные армии)'
Warhammer
White Box Fantastic Medieval Adventure Game'
Witcher (Ведьмак)'
Year Zero'
Zweihander.pdf
Адреналин
Арргхх
Блэкбердпай
В свете призрачной звезды.pdf'
В темнейшие закоулки.pdf'
Дневник Дрёммеби.pdf'
Дома Чистокровных.pdf'
Зомби Апокалипсис.pdf'
Зомби - Мёртвый мир'
Интегри
Колония
Корни
Королевство Ничто.pdf'
Культ кота'
Лавикандия
Легенды древнего мира'
Маленькие игры'
Мастера духов'
Мертвые Земли'
Мечтатель, Чародей, Обречённый'
Микроскоп.pdf
Мир великого дракона'
Не ходите в зимний лес.pdf'
Нити судьбы'
Ночь в дождливом октябре'
Обитатели холмов.pdf'
Обрез
Охотники на чудовищ.pdf'
Палеолит.pdf
Перекресток миров'
Праздник в Ред-Хуке.pdf'
Пробуждение
Пси-Ран
Рейнджеры Глубокой Тени.pdf'
Сальвеблюз
Седьмое море'
Сеттинги Лазаря_Добрые люди'
Системонезависимые приключения'
Союз Серокрылых.pdf'
Тайны Эхосферы'
Твистед терра'
Теперь я не забуду'
Тридцатиминутные герои'
Уборщики.pdf
Феодал ролеплейнг'
Фиаско
Чорное Кромсалово 2.pdf'
Шиноби
Эра водолея'
Эфир и Шестерни.pdf'
     */
];

export const getItems = handler(async () => {
    const query = {};
    return Game.find(query).sort('name');
});

export const addItem = handler(async (req: Request) => {
    const record = new Game(req.body);
    return await record.save();
});

export const getItem = handler(async (req: Request) => Game.findOne({ slug: req.params.slug }));

export const updateItem = handler((req: Request) => Game.findByIdAndUpdate(req.params.id, req.body));

export const removeItem = handler((req: Request) => Game.findByIdAndDelete(req.params.id, req.body));
